//
//  BaseXIBViewController.swift
//  AstrologyCustomControl
//
//  Created by Massimo Savino on 2016-07-11.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit

class BaseXIBViewController: UIViewController {
    
    // MARK: - Initialization
    
    init() {
        super.init(nibName: String(self.dynamicType), bundle: NSBundle.mainBundle())
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }

    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
